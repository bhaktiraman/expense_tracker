from flask import render_template, flash, redirect, request, url_for, g, json, jsonify, send_file
from app import app, db, celery
from app.forms import LoginForm, RegistrationForm, AddContactForm, AddTransactionForm, EditProfileForm, SearchForm
from flask_login import logout_user, login_required, current_user, login_user
from werkzeug.urls import url_parse
from app.models import User, Contact, Transaction
from sqlalchemy.sql import func
from app.task import create_file_for_download
import psycopg2

@app.route('/')
@app.route("/login", methods = ['GET', 'POST'])
def login():
    form = LoginForm()
    if current_user.is_authenticated:
        return redirect(url_for('profile',username=current_user.username))
    form = LoginForm()
    if form.validate_on_submit():
        user = User.query.filter_by(username=form.username.data).first()
        if user is None or not user.check_password(form.password.data):
            flash('Invalid Username or Password')
            return redirect(url_for('login'))
        login_user(user, remember=form.remember_me.data)        
        next_page = request.args.get('next')
        if not next_page or url_parse(next_page).netloc != '':
            next_page = url_for('profile',username=current_user.username)
        return redirect(next_page)
    return render_template("login.html", title='Login', form=form)

@app.route("/register", methods = ['GET', 'POST'])
def register():
    if current_user.is_authenticated:
        return redirect(url_for('profile',username=current_user.username))
    form = RegistrationForm()
    if form.validate_on_submit():
        user = User(username=form.username.data, name=form.name.data, email=form.email.data)
        user.set_password(form.password.data)
        db.session.add(user)
        db.session.commit()
        flash('Congratulations, you are now a registered user!')

        login_user(user)
        next_page = request.args.get('next')
        if not next_page or url_parse(next_page).netloc != '':
            next_page = url_for('profile',username=user.username)
        return redirect(next_page)

    return render_template("register.html", title='Register', form=form)

@app.route("/profile", methods = ['GET', 'POST'])
@login_required
def profile():
    net_amount=[0,]
    net_amount=Transaction.query.filter_by(user_id=current_user.id).\
        with_entities(func.sum(Transaction.amount).label("amount")).first()
    return render_template("profile.html", title='Profile', net_amount=net_amount[0])

@app.route("/contacts", methods = ['GET', 'POST'])
@login_required
def contacts():
    form = AddContactForm()
    if form.validate_on_submit():
        contact = Contact(name=form.name.data, email=form.email.data, contact=current_user)
        db.session.add(contact)
        db.session.commit()
        form.name.data = ''
        form.email.data = ''
        flash('Contact is added successfully')
        return redirect(url_for('transactions', contact_id=contact.id))
    contacts = Contact.query.filter_by(user_id=current_user.id)
    amount = {}
    trans = Transaction.query.filter_by(user_id=current_user.id)
    for each_trans in trans:
        if each_trans.contact_id not in amount:
            amount[each_trans.contact_id] = 0
        amount[each_trans.contact_id] += each_trans.amount
    return render_template("contacts.html", title='Contacts', form=form, contacts=contacts, amount=amount)

@app.route("/delete_contact/<contact_id>")
@login_required
def delete_contact(contact_id):
    contact = Contact.query.filter_by(id=contact_id).first()
    transactions = Transaction.query.filter_by(contact_id=contact_id).all()
    print(transactions)
    if contact:
        if contact.user_id == current_user.id:
            db.session.delete(contact)
            if len(transactions) > 0:
                for transaction in transactions:
                    print(transaction)
                    db.session.delete(transaction)
            db.session.commit()
            return redirect(url_for('contacts'))
        return redirect(url_for('error', error_id = 403))
    return redirect(url_for('error', error_id = 404))


@app.route("/transactions/<contact_id>", methods = ['GET', 'POST'])
@login_required
def transactions(contact_id):
    amount=[0,]
    trans = Transaction.query.filter_by(contact_id=contact_id)
    amount =Transaction.query.filter_by(contact_id=contact_id).\
        with_entities(func.sum(Transaction.amount).label("amount")).first()
    contacts = Contact.query.filter_by(id=contact_id).first()
    form = AddTransactionForm()
    if form.validate_on_submit():
        transaction = Transaction(reason=form.reason.data, amount=form.amount.data,
                        user_id=current_user.id, contact_id=contact_id)
        db.session.add(transaction)
        db.session.commit()
        flash('Transaction has been added successfully')
        return redirect(url_for('transactions', contact_id=contact_id))
    transactions=Transaction.query.filter_by(user_id=current_user.id, contact_id=contact_id)
    return render_template("transactions.html", contact_id=contact_id, title='Transactions', contacts=contacts, 
                            transactions=transactions, form=form, amount=amount[0])

@app.route("/delete_transaction/<transaction_id>")
@login_required
def delete_transaction(transaction_id):
    transaction = Transaction.query.filter_by(id=transaction_id).first()
    if transaction:
        if transaction.user_id == current_user.id:
            contact_id = transaction.contact_id
            db.session.delete(transaction)
            db.session.commit()
            return redirect(url_for('transactions', contact_id=contact_id))
        return redirect(url_for('error', error_id = 403))
    return redirect(url_for('error', error_id = 404))

@app.route("/edit-profile", methods = ['GET', 'POST'])
@login_required
def edit_profile():
    form = EditProfileForm()
    if request.method == 'POST':
        print("Edit Profile function call")
        current_user.name = form.name.data
        db.session.commit()
        flash('Your changes have been saved.')
        return redirect(url_for('profile'))
    elif request.method == 'GET':
        form.name.data = current_user.name
    return render_template("edit_profile.html", title='Edit Profile', form=form)

@app.route('/logout')
@login_required
def logout():
    logout_user()
    flash('Please login to access this web app.')
    return redirect(url_for('login'))

@app.before_request
def before_request():
    g.search_form = SearchForm()

@app.route('/search')
@login_required
def search():
    contacts, total = Contact.search(g.search_form.q.data)
    transactions, total2 = Transaction.search(g.search_form.q.data)

    return render_template('search.html', title=('Search'), contacts=contacts, 
            total=total, transactions=transactions, total2=total2)

@app.route('/download_contents', methods = ['GET', 'POST'])
@login_required
def download_contents():
    task = create_file_for_download.apply_async((current_user.id,))
    return jsonify({}), {'Location': url_for('check_download_content_progress', task_id=task.id)}

@app.route('/download_status/<task_id>')
@login_required
def check_download_content_progress(task_id):
    task = create_file_for_download.AsyncResult(task_id)
    response = {
        'state': task.state,
    }
    return jsonify(response)

@app.route('/download', methods = ['GET', 'POST'])
@login_required
def download():
    return send_file('../data.json', attachment_filename=f'{current_user.name}.txt', as_attachment=True, cache_timeout=0)

@app.route('/error/<error_id>', methods = ['GET', 'POST'])
def error(error_id):
    if error_id=='403':
        return render_template('errors/403.html', title='Error')
    elif error_id=='404':
        return render_template('errors/404.html', title='Error')
    return render_template('errors/500.html', title='Error')
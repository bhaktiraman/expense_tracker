from flask import Flask
from config import Config
from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate
from flask_login import LoginManager
from elasticsearch import Elasticsearch
from celery import Celery
import certifi


app = Flask(__name__)
app.config['CELERY_BROKER_URL'] = 'redis://localhost:6379/0'
app.config['CELERY_RESULT_BACKEND'] = 'redis://localhost:6379/0'
app.config.from_object(Config)
db = SQLAlchemy(app)
migrate = Migrate(app, db)
login = LoginManager(app)
login.login_view = 'login'
app.elasticsearch = Elasticsearch(app.config['ELASTICSEARCH_URL'], \
    use_ssl=True, ca_certs=certifi.where(),http_auth=('elastic','EzfefD5ahJneWwLOPLMM181L')) \
    if app.config['ELASTICSEARCH_URL'] else None
# app.elasticsearch = Elasticsearch([app.config['ELASTICSEARCH_URL']]) \
#         if app.config['ELASTICSEARCH_URL'] else None
celery = Celery(app.name, broker=app.config['CELERY_BROKER_URL'])
celery.conf.update(app.config)


from app import routes, models
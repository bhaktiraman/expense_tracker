from celery import Celery
from app import db, celery
from flask import json, Flask
from app.models import Transaction, Contact, User
from config import Config
from flask_sqlalchemy import SQLAlchemy
from sqlalchemy.sql import func


@celery.task()
def create_file_for_download(user_id):
    app = Flask(__name__)
    db = SQLAlchemy(app)
    app.config.from_object(Config)
    with app.app_context():
        users = User.query.filter_by(id=user_id)
        for user in users:
            data1={}
            net_amount=Transaction.query.filter_by(user_id=user.id).\
                with_entities(func.sum(Transaction.amount).label("amount")).first()
            data1={'Username': user.username, 'Name': user.name,\
                'E-mail': user.email, 'Net Amount': net_amount[0]}
        
        data2 = {}
        contacts = Contact.query.filter_by(user_id=user.id)
        trans = Transaction.query.filter_by(user_id=user.id)

        for contact in contacts:
            amount = 0
            if contact.name not in data2:
                data2[contact.name] = {}
            for each_trans in trans:
                if each_trans.contact_id == contact.id:
                    data2[contact.name][each_trans.reason] = each_trans.amount                 
                    amount += each_trans.amount
            data2[contact.name]['Total'] = amount             
        
        data = {}
        data['User'] = data1
        data['Contacts'] = data2
        file = open('data.json','w')
        file.write(json.dumps(data))
        file.close()

    return 'Done'


    # select c.name, sum(t.amount) as balance from 
    #     contact as c inner join on 
    #     transaction as t where 
    #     c.user_id=user_id and t.user_id=user_id
    #     group by c.name
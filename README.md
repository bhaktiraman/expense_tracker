Expense_Tracker
===============

> A kind of Dues Management.

> Flask Project

**Core features :**
	
	0. Register and Login for each user.

	1. Add Contact (eg. Friend's Name, Shopkeeper, Salary, Mess etc.)
	
	2. Add Transaction inside Contact (eg. UPI Transfer, Paid in cash, For Month - June, for <something>)
	
	3. Amount for each Contact (either to pay '-' or to get paid '+')
	
	4. Display Sum of all Transaction's amount for each Contact in -ve or +ve.
	
	5. Display Sum of all Contact's for each user [Net expense of the user].

	6. Elastic search in nav-bar.

> Please go through the `deploy.sh` and `manual_installations_with_deploy_dot_sh.md` for Production deployment

> To start the servers which are required to run for running of this web app, commands are in
`servers_start_stop_commands.md`
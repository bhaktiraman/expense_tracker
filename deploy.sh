git clone https://gitlab.com/brd.mb/expense_tracker.git
sudo apt-get update
sudo apt-get install python3-venv
python3 -m venv .env
sudo apt install redis-server
sudo systemctl restart redis-service
sudo apt install postgresql postgresql-contrib
pip install -r requirements.txt
sudo apt-get install nginx
pip install gunicorn
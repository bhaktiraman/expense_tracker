Required Commands
=================

> To connect to EC2 System ->
Format - `ssh -i "<Key pair name>" ubuntu@<Public DNS>` 
Key pair name example- `flasksiteec2launch.pem`
Public DNS example- `ec2-13-233-113-158.ap-south-1.compute.amazonaws.com`
Example - `ssh -i "flasksiteec2launch.pem" ubuntu@ec2-13-233-108-63.ap-south-1.compute.amazonaws.com`

> Clone the git repo:
`git clone https://gitlab.com/brd.mb/expense_tracker.git`
`git config --global user.email "bhakti.raman@mountblue.io"`
`git config --global user.name "Bhakti Raman"`

> To install virtualenv ->
1. `sudo apt-get update`
2. `sudo apt-get install python3-venv`
3. to create virtual environment `python3 -m venv venv`

> To install Redis ->
`sudo apt install redis-server`
`sudo systemctl restart redis-service` - to restart redis-services

> To install Postgres ->
`sudo apt install postgresql postgresql-contrib`

> Create Table Manually ->
`sudo su` - moving to root mode
`su postgres` - to connect with postgresql
`psql` - to start postgresql shell
`\password` - to change the password
`CREATE DATABASE expense_tracker;` - to create table
`flask db init`
`flask db migrate`
`flask db upgrade`

> Elasticsearch Reindexing ->
(on flask shell) `Contact.reindex()` and `Transaction.reindex()`

> Install the requirements ->
`pip install -r requirements.txt`

> Install Nginx ->
`sudo apt-get install nginx`

> Install Gunicorn ->
`pip install gunicorn`

> May You need to run these two commands if error reflects:
`export FLASK_APP=main.py`
`sudo apt-get upgrade python3-pip`

> Configuration of Nginx ->
`sudo vim /etc/nginx/conf.d/virtual.conf`
Paste the following code in that virtual.conf file:
```
server {
    listen       80;
    location / {
        proxy_pass http://127.0.0.1:8000;
    }
}
```
> Restart the nginx web server ->
`sudo nginx -t`
`sudo service nginx restart`
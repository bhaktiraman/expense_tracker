Servers start-stop commands
===========================

> Flask Server
Start - `flask run` or for ec2 `flask run --host=0.0.0.0`
Stop - `Ctrl + c`

> Postgresql Server
Start - `sudo service postgresql start`
Stop - `sudo service postgresql stop`

> ElasticSearch Server
Start - `sudo -i service elasticsearch start`
Stop - `sudo -i service elasticsearch stop`

> Redis Server
Start - `sudo systemctl start redis` or `redis-stable/src/redis-server`
Stop - `sudo systemctl stop redis` or `Ctrl + c`
status - `sudo systemctl status redis`

> Celery Server (A worker)
Start - `celery -A app.task worker --loglevel=info`
Stop - `Ctrl + c`

> Gunicorn ‘Green Unicorn’
Start - `gunicorn app:app -b localhost:8000 &`
Stop - ``

> Restart the Nginx web server
`sudo nginx -t`
`sudo service nginx restart`